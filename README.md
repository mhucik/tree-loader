# tree-loader

Load tree from csv file and make simple operations on loaded tree

# How to start?
Just run this command:
```shell script
docker-compose run phpserver composer install
```

Application have only console access. So you just have to run `docker-compose exec phpserver php bin/console` and pick one of implemented commands.
# Commands examples

1. Load tree from file and print it to console
    ```shell script
    docker-compose run phpserver php bin/console app:load-and-print temp/file.csv
    ```
2. Load tree from file and save it back to new file
    ```shell script
     docker-compose run phpserver php bin/console app:load-and-save temp/file.csv temp/export.csv
    ```
3. Load tree from file and move specified subtree with 'excercise' root to subtree with 'business-categories' root
    ```shell script
     docker-compose run phpserver php bin/console app:move-subtree-and-print temp/file.csv exercises business-categories
    ```
4. Load tree from file and sum up values from subtree with 'exercises' root
    ```shell script
    docker-compose run phpserver php bin/console app:sum-form-node temp/file.csv exercises
    ```