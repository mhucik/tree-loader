<?php declare(strict_types = 1);

namespace App;

use Nette\DI\Compiler;
use Nette\DI\CompilerExtension;
use Nette\DI\Container;
use Nette\DI\Extensions\ExtensionsExtension;

final class Bootstrap
{
    public static function boot(bool $debugMode): Container
    {
        $loader = new \Nette\DI\ContainerLoader(__DIR__ . '/../temp/container', $debugMode);
        $class = $loader->load(static function(Compiler $compiler): void {
            $compiler->loadConfig(__DIR__ . '/config/config.neon');

            /** @var CompilerExtension $extension */
            $extension = (new \ReflectionClass(ExtensionsExtension::class))->newInstanceArgs([]);
            $compiler->addExtension('extensions', $extension);
        });

        \Tracy\Debugger::$strictMode = TRUE;
        \Tracy\Debugger::enable(!$debugMode, __DIR__ . '/../log/');
        \Tracy\Bridges\Nette\Bridge::initialize();

        return new $class;
    }
}
