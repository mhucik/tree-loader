<?php declare(strict_types = 1);

namespace App\Builder;

use App\Entity\Node;

final class TreeBuilder
{
    /**
     * @param array<mixed> $elements
     * @return Node[]
     */
    public function buildTree(array &$elements, ?Node $parent = NULL): array
    {
        $nodes = [];

        foreach ($elements as $id => $element) {
            if ( ! $parent && (int) $element['parent_id'] !== 0) {
                continue;
            }

            if ($parent && (int) $element['parent_id'] !== $parent->getId()) {
                continue;
            }

            $node = new Node(
                (int) $element['id'],
                $parent,
                $element['identifier'],
                (int) $element['value'],
            );

            $children = $this->buildTree($elements, $node);

            if ($children) {
                $node->setChildren($children);
            }

            $nodes[] = $node;
            unset($elements[$id]);
        }

        return $nodes;
    }
}
