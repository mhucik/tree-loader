<?php declare(strict_types = 1);

namespace App\Command;

use App\File\FileLoader;
use App\Visitor\PrintTreeVisitor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class LoadAndPrintTreeCommand extends Command
{
    private const FILE_PATH_ARG = 'filePath';

    private FileLoader $fileLoader;

    private PrintTreeVisitor $printTreeVisitor;

    public function __construct(
        string $name,
        FileLoader $testClass,
        PrintTreeVisitor $printTreeVisitor
    )
    {
        parent::__construct($name);
        $this->fileLoader = $testClass;
        $this->printTreeVisitor = $printTreeVisitor;
    }


    protected function configure(): void
    {
        parent::configure();
        $this->setDescription('Load data from file into tree structure and print them');
        $this->addArgument(self::FILE_PATH_ARG, InputArgument::REQUIRED, 'path to loaded file');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $path */
        $path = $input->getArgument(self::FILE_PATH_ARG);
        $tree = $this->fileLoader->loadFromFile($path);

        $this->printTreeVisitor->visit(reset($tree));

        return Command::SUCCESS;
    }
}
