<?php declare(strict_types = 1);

namespace App\Command;

use App\File\FileLoader;
use App\File\FileWriter;
use App\Visitor\PrepareForExportVisitor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class LoadAndSaveCommand extends Command
{
    private const FILE_PATH_ARG = 'filePath';
    private const FILE_EXPORT_PATH_ARG = 'fileExportPath';

    private FileLoader $fileLoader;

    private FileWriter $fileWriter;

    private PrepareForExportVisitor $prepareForExportVisitor;

    public function __construct(
        string $name,
        FileLoader $fileLoader,
        FileWriter $fileWriter,
        PrepareForExportVisitor $prepareForExportVisitor
    )
    {
        parent::__construct($name);
        $this->fileLoader = $fileLoader;
        $this->fileWriter = $fileWriter;
        $this->prepareForExportVisitor = $prepareForExportVisitor;
    }


    protected function configure(): void
    {
        parent::configure();
        $this->setDescription('Load data from file into tree structure and save them back to file');
        $this->addArgument(self::FILE_PATH_ARG, InputArgument::REQUIRED, 'path to loaded file');
        $this->addArgument(self::FILE_EXPORT_PATH_ARG, InputArgument::REQUIRED, 'path to file for export');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $path */
        $path = $input->getArgument(self::FILE_PATH_ARG);
        $tree = $this->fileLoader->loadFromFile($path);

        $node = reset($tree);

        /** @var string $pathToWrite */
        $pathToWrite = $input->getArgument(self::FILE_EXPORT_PATH_ARG);

        $this->fileWriter->writeToFile($this->prepareForExportVisitor->visit($node), $pathToWrite);

        return Command::SUCCESS;
    }


}
