<?php declare(strict_types = 1);

namespace App\Command;

use App\Entity\Node;
use App\Exception\NodeNotFound;
use App\File\FileLoader;
use App\Visitor\FindInTreeVisitor;
use App\Visitor\PrintTreeVisitor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class MoveSubtreeAndPrintCommand extends Command
{
    private const FILE_PATH_ARG = 'filePath';
    private const MOVE_FROM_IDENTIFIER = 'from';
    private const MOVE_TO_IDENTIFIER = 'to';

    private FileLoader $fileLoader;

    private PrintTreeVisitor $printTreeVisitor;

    private FindInTreeVisitor $findInTreeVisitor;

    public function __construct(
        string $name,
        FileLoader $fileLoader,
        PrintTreeVisitor $printTreeVisitor,
        FindInTreeVisitor $findInTreeVisitor
    )
    {
        parent::__construct($name);
        $this->fileLoader = $fileLoader;
        $this->printTreeVisitor = $printTreeVisitor;
        $this->findInTreeVisitor = $findInTreeVisitor;
    }


    protected function configure(): void
    {
        parent::configure();
        $this->setDescription('Load data from file into tree structure, move subtree and print new tree');
        $this->addArgument(self::FILE_PATH_ARG, InputArgument::REQUIRED, 'path to loaded file');
        $this->addArgument(self::MOVE_FROM_IDENTIFIER, InputArgument::REQUIRED, 'Identifier of subnode to cut from tree');
        $this->addArgument(self::MOVE_TO_IDENTIFIER, InputArgument::REQUIRED, 'Identifier of subnode to push cutted tree as a new child');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var string $path */
        $path = $input->getArgument(self::FILE_PATH_ARG);
        $tree = $this->fileLoader->loadFromFile($path);

        /** @var Node $node */
        $node = reset($tree);
        /** @var string $identifierFrom */
        $identifierFrom = $input->getArgument(self::MOVE_FROM_IDENTIFIER);
        /** @var string $identifierTo */
        $identifierTo = $input->getArgument(self::MOVE_TO_IDENTIFIER);

        try {
            $exercises = $this->findInTreeVisitor->visit($node, $identifierFrom);
        } catch (NodeNotFound $e) {
            $output->writeln(sprintf('Node with identifier "%s" not found.', $identifierFrom));

            return Command::SUCCESS;
        }

        $exercisesParent = $exercises->getParent();
        $exercisesParent->removeChild($exercises);

        try {
            $businessCategories = $this->findInTreeVisitor->visit($node, $identifierTo);
        } catch (NodeNotFound $e) {
            $output->writeln(sprintf('Node with identifier "%s" not found.', $identifierTo));

            return Command::SUCCESS;
        }

        $businessCategories->addChild($exercises);

        $this->printTreeVisitor->visit($node);

        return Command::SUCCESS;
    }


}
