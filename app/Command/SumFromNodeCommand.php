<?php declare(strict_types = 1);

namespace App\Command;

use App\Exception\NodeNotFound;
use App\File\FileLoader;
use App\Visitor\FindInTreeVisitor;
use App\Visitor\NodeValueSumVisitor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class SumFromNodeCommand extends Command
{
    private const FILE_PATH_ARG = 'filePath';
    private const COUNT_FROM_IDENTIFIER = 'from';

    private FileLoader $fileLoader;

    private NodeValueSumVisitor $nodeValueSumVisitor;

    private FindInTreeVisitor $findInTreeVisitor;

    public function __construct(
        string $name,
        FileLoader $fileLoader,
        NodeValueSumVisitor $nodeValueSumVisitor,
        FindInTreeVisitor $findInTreeVisitor
    )
    {
        parent::__construct($name);
        $this->fileLoader = $fileLoader;
        $this->nodeValueSumVisitor = $nodeValueSumVisitor;
        $this->findInTreeVisitor = $findInTreeVisitor;
    }


    protected function configure(): void
    {
        parent::configure();
        $this->setDescription('Load data from file into tree structure and sum up total value of subtree identified by entered identifier.');
        $this->addArgument(self::FILE_PATH_ARG, InputArgument::REQUIRED, 'path to loaded file');
        $this->addArgument(self::COUNT_FROM_IDENTIFIER, InputArgument::REQUIRED, 'Count values sum from node with this identifier');
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $identifier */
        $identifier = $input->getArgument(self::COUNT_FROM_IDENTIFIER);
        /** @var string $path */
        $path = $input->getArgument(self::FILE_PATH_ARG);
        $tree = $this->fileLoader->loadFromFile($path);

        $node = reset($tree);

        try {
            $countFromNode = $this->findInTreeVisitor->visit($node, $identifier);
        } catch (NodeNotFound $e) {
            $output->writeln(sprintf('Node with identifier "%s" not found.', $identifier));

            return Command::SUCCESS;
        }

        $totalSum = $this->nodeValueSumVisitor->visit($countFromNode);

        $output->writeln(sprintf('Total sum of %s child nodes is %s', $identifier, $totalSum));

        return Command::SUCCESS;
    }


}
