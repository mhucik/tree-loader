<?php declare(strict_types = 1);

namespace App\Entity;

use App\Visitor\Visitor;

final class Node
{
    private int $id;

    private string $identifier;

    private ?Node $parent;

    private int $value;

    /**
     * @var Node[]
     */
    private array $children = [];

    public function __construct(
        int $id,
        ?Node $parent,
        string $identifier,
        int $value
    )
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->parent = $parent;
        $this->value = $value;
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function getParent(): ?Node
    {
        return $this->parent;
    }


    /**
     * @param Node[] $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }


    public function removeChild(Node $child): void
    {
        foreach ($this->children as $key => $myChild) {
            if ($child === $myChild) {
                unset($this->children[$key]);
            }
        }

        $this->children = \array_values($this->children);

        $child->setParent(NULL);
    }


    public function addChild(Node $child): void
    {
        $child->setParent($this);
        $this->children[] = $child;
    }


    public function setParent(?Node $parent): void
    {
        $this->parent = $parent;
    }


    /**
     * @return array<string, mixed>
     */
    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent ? $this->parent->getId() : '',
            'identifier' => $this->identifier,
            'value' => $this->value,
        ];
    }


    /**
     * @return mixed
     */
    public function accept(Visitor $visitor, ?string $identifier = NULL)
    {
        return $visitor->visit($this, $identifier);
    }


    public function getDepth(): int
    {
        $parentNode = $this->parent;

        if ( ! $parentNode) {
            return 0;
        }

        return $parentNode->getDepth() + 1;
    }


    /**
     * @return Node[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }


    public function getIdentifier(): string
    {
        return $this->identifier;
    }


    public function getValue(): int
    {
        return $this->value;
    }
}
