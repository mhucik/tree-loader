<?php declare(strict_types = 1);

namespace App\File;

use App\Builder\TreeBuilder;
use League\Csv\Reader;

final class FileLoader
{
    private TreeBuilder $treeBuilder;

    public function __construct(
        TreeBuilder $treeBuilder
    )
    {
        $this->treeBuilder = $treeBuilder;
    }


    /**
     * @return array<mixed>
     * @throws \League\Csv\Exception
     */
    public function loadFromFile(string $path): array
    {
        $csv = Reader::createFromPath($path);

        $csv->setHeaderOffset(0);

        $nodes = iterator_to_array($csv);
        $parsedNodes = $this->treeBuilder->buildTree($nodes);

        return $parsedNodes;
    }
}
