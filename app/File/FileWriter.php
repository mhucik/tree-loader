<?php declare(strict_types = 1);

namespace App\File;

use League\Csv\Writer;

final class FileWriter
{
    /**
     * @param array<int, mixed> $records
     * @param string $path
     * @throws \League\Csv\CannotInsertRecord
     */
    public function writeToFile(array $records, string $path): void
    {
        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne(['id', 'parent_id', 'identifier', 'value']);

        $csv->insertAll($records);

        ob_start();
        $csv->output();
        $records = ob_get_contents();

        ob_end_clean();

        file_put_contents($path, $records);
    }
}
