<?php declare(strict_types = 1);

namespace App\Visitor;

use App\Entity\Node;
use App\Exception\NodeNotFound;

final class FindInTreeVisitor implements Visitor
{
    public function visit(Node $node, ?string $identifier = NULL)
    {
        if ($node->getIdentifier() === $identifier) {
            return $node;
        }

        foreach ($node->getChildren() as $child) {
            try {
                return $child->accept($this, $identifier);
            } catch (NodeNotFound $exception) {
                continue;
            }
        }

        throw new NodeNotFound();
    }

}
