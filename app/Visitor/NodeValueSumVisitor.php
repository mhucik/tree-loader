<?php declare(strict_types = 1);

namespace App\Visitor;

use App\Entity\Node;

final class NodeValueSumVisitor implements Visitor
{
    public function visit(Node $node, ?string $identifier = NULL)
    {
        $sum = 0;

        foreach ($node->getChildren() as $child) {
            $sum += $child->accept($this, $identifier);
        }

        $sum += $node->getValue();

        return $sum;
    }

}
