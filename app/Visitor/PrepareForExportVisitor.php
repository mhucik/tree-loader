<?php declare(strict_types = 1);

namespace App\Visitor;

use App\Entity\Node;

final class PrepareForExportVisitor implements Visitor
{
    public function visit(Node $node, ?string $identifier = NULL)
    {
        $nodes = [];

        foreach ($node->getChildren() as $child) {
            $nodes = \array_merge($nodes, $child->accept($this));
        }

        $nodes[] = $node->serialize();

        return $nodes;
    }

}
