<?php declare(strict_types = 1);

namespace App\Visitor;

use App\Entity\Node;

final class PrintTreeVisitor implements Visitor
{
    public function visit(Node $node, ?string $identifier = NULL)
    {
        echo str_pad('-> ' . $node->getId(), 6, ' ', STR_PAD_RIGHT);

        $childrenCount = count($node->getChildren());
        foreach ($node->getChildren() as $child) {
            $child->accept($this);
            $childrenCount--;

            if ($childrenCount > 0) {
                echo "\n";
                echo str_repeat(str_pad(" ", 6, ' ', STR_PAD_LEFT), $child->getDepth());
            }
        }
    }

}
