<?php declare(strict_types = 1);

namespace App\Visitor;

use App\Entity\Node;

interface Visitor
{
    /**
     * @return mixed
     */
    public function visit(Node $node, ?string $identifier = NULL);
}
